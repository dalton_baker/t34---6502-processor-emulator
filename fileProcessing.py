def processFile(fileName):
    returnData = []

    #open the file, grab the data, and close the file
    try:
        file = open(fileName, "r")
        fileData = file.readlines()
        file.close()
    except:
        print("T34 START ERROR: File does not exist or cannot be opened")
        exit(1)

    #check that the file is in the correct format
    checkFile(fileData)

    #read it line by line
    for line in fileData:
        #this is so the program will ignore empty lines
        if line.strip():
            lineData = decodeLine(line)
            #if the code is data, add it to the list
            if lineData[0] == "00":
                returnData.append(lineData[1:3])
    return returnData

#This will make sure a file is in the correct format and the data is valid
def checkFile(fileData):
    #grab each line from the file and return it's contents
    for line in fileData:
        #this is so the program will ignore empty lines
        if line.strip():
            dataLen = int(line[1:3], 16)*2
            #If the first character isn't ":" quit
            if not line[0] == ":":
                print("T34 START ERROR: File format not valid")
                exit(1)
            #check that there was the right number of chars in the line
            if len(line) < dataLen + 12:
                print("T34 START ERROR: File missing data")
                exit(1)
            #If the file contains non hex characters, exit the program
            if not checkHex(line[1:dataLen+11]):
                print("T34 START ERROR: File data contains non-hex characters")
                exit(1)
            #If the checksum isn't correct, exit the program
            if not checkChecksum(line[1:dataLen+9], line[dataLen+9:dataLen+11]):
                print("T34 START ERROR: File's checksum did not add up")
                exit(1)

#This will pull a line apart into its separate variables
def decodeLine(lineData):
    returnList = []
    #grab the Record Type
    returnList.append(lineData[7:9])
    #grab the address for the data
    returnList.append(lineData[3:7])
    #grab the data
    returnList.append(lineData[9:int(lineData[1:3], 16)*2+9])

    return returnList

#This will check if a string is in hex format
def checkHex(hexStr):
    try:
        #try converting to int
        int(hexStr, 16)
        return True
    except:
        #if nothing works return false
        return False

def checkChecksum(data, checksum):
    sumOfBytes = 0
    #get a sum of all the bytes in the data
    for i in bytearray.fromhex( data ):
        sumOfBytes += i;

    #convert to 2's compliment
    sumOfBytes = sumOfBytes & 255
    sumOfBytes = ((1 << 8) - sumOfBytes)
    sumOfBytes = sumOfBytes & 255

    #compare to the checksum
    return sumOfBytes == int(checksum, 16)
