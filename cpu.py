from memory import Memory

def byteToString(byte):
    hexOut = ""
    if byte < 16:
        hexOut = hexOut + "0"
    hexOut = hexOut + str( hex( byte ) )[2:].upper()
    return hexOut

def twosComp(byte):
    byte = onesComp(byte)
    return byte + 1

def onesComp(byte):
    return (byte ^ 0b11111111) & 0B11111111

#This is the monitor object, it will handle all of the input and output
#of the T34 machine
class CPU:
    def __init__(self, memory):
        self.PC = 0
        self.AC = 0
        self.XR = 0
        self.YR = 0
        self.SR = 32
        self.SP = 255
        self.mem = memory
        self.output = ""

    def runProg(self, start):
        #Print the key
        print(" PC  OPC  INS   AMOD OPRND  AC XR YR SP NV-BDIZC")

        #set the program counter
        self.PC = int( start, 16 )
        self.AC = 0
        self.XR = 0
        self.YR = 0
        self.SR = 32
        self.SP = 255

        while True:
            #build the output
            self.output = " {: <4} ".format(byteToString(self.PC))
            nextInst = self.__getNextMem()
            self.output += byteToString(nextInst)
            ##call the instruction
            self.__decodeOpcode(nextInst)
            #ptint output
            print(self.output)
            if nextInst is 0X00:
                break;

    def __getMem(self, loc):
        return self.mem.returnSingle(loc)

    def __setMem(self, loc, data):
        self.mem.setMem(loc, data)

    def __getNextMem(self):
        self.PC += 1
        return self.__getMem(self.PC - 1)

    def __addOutput(self, inst, mode, op1, op2):
        self.output += "  {: >3}  {: >5} {: >2} {: >2}  ".format(inst, mode, op1, op2)
        self.output += "{: >2} {: >2} ".format(byteToString(self.AC), byteToString(self.XR))
        self.output += "{: >2} {: >2} ".format(byteToString(self.YR), byteToString(self.SP))
        self.output += format(self.SR, '0{}b'.format(8))

    def __pushOnStack(self, data):
        self.__setMem(0X0100+self.SP, data)
        self.SP -= 1
        self.SP = self.SP & 255

    def __pullFromStack(self):
        self.SP += 1
        self.SP = self.SP & 255
        return self.__getMem(0X0100+self.SP)

    #Check the number for a sign bit of a result
    def __setN(self, res):
        if res & 128 == 0: self.SR = self.SR & 127
        else: self.SR = self.SR | 128

    #Check the sign bit of the 2 numbers and the sign bit of the result
    # if the 2 numbers have the same sign bit, and the results sign bit is
    # different, set the V flag
    def __setV(self, num1, num2, res):
        if (num1 & 128 == 0) and (num2 & 128 == 0) and (res & 128 == 128): self.SR = self.SR | 0B01000000
        elif (num1 & 128 == 128) and (num2 & 128 == 128) and (res & 128 == 0): self.SR = self.SR | 0B01000000
        else: self.SR = self.SR & 0B10111111

    #set if any result is 0
    def __setZ(self, res):
        if res == 0: self.SR = self.SR | 0B00000010
        else: self.SR = self.SR & 0B11111101

    #set if the a subtraction will result in a borrow
    #num1 MUST be an 8 bit number, if it is signed, leave it as it is!!!
    #num2 MUST be the number being subtracted!!
    def __setC(self, num1, num2="Addition"):
        if num2 is not "Addition":
            if (num1 & 128 == 0) and (num2 & 128 == 128) and (num1+twosComp(num2) > 127) : self.SR = self.SR | 0B00000001
            elif (num1 & 128 == 128) and (num2 & 128 == 0) and (twosComp(num1)+num2 > 128) : self.SR = self.SR | 0B00000001
            else: self.SR = self.SR & 0B11111110
        else:
            if num1 & 256 == 0: self.SR = self.SR & 0B11111110
            else: self.SR = self.SR | 0B00000001

    def __decodeOpcode(self, opcode):
        #impl & A
        if opcode == 0x00: self.__BRK()
        elif opcode == 0x0A: self.__ASL()
        elif opcode == 0x18: self.__CLC()
        elif opcode == 0xD8: self.__CLD()
        elif opcode == 0x58: self.__CLI()
        elif opcode == 0xB8: self.__CLV()
        elif opcode == 0xCA: self.__DEX()
        elif opcode == 0x88: self.__DEY()
        elif opcode == 0xE8: self.__INX()
        elif opcode == 0xC8: self.__INY()
        elif opcode == 0x4A: self.__LSR()
        elif opcode == 0xEA: self.__NOP()
        elif opcode == 0x48: self.__PHA()
        elif opcode == 0x08: self.__PHP()
        elif opcode == 0x68: self.__PLA()
        elif opcode == 0x28: self.__PLP()
        elif opcode == 0x2A: self.__ROL()
        elif opcode == 0x6A: self.__ROR()
        elif opcode == 0x38: self.__SEC()
        elif opcode == 0xF8: self.__SED()
        elif opcode == 0x78: self.__SEI()
        elif opcode == 0xAA: self.__TAX()
        elif opcode == 0xA8: self.__TAY()
        elif opcode == 0xBA: self.__TSX()
        elif opcode == 0x8A: self.__TXA()
        elif opcode == 0x9A: self.__TXS()
        elif opcode == 0x98: self.__TYA()
        elif opcode == 0x60: self.__RTS()
        elif opcode == 0x40: self.__RTI()
        #imm (#)
        elif opcode == 0x69: self.__ADC_imm()
        elif opcode == 0x29: self.__AND_imm()
        elif opcode == 0xC9: self.__CMP_imm()
        elif opcode == 0xE0: self.__CPX_imm()
        elif opcode == 0xC0: self.__CPY_imm()
        elif opcode == 0x49: self.__EOR_imm()
        elif opcode == 0xA9: self.__LDA_imm()
        elif opcode == 0xA2: self.__LDX_imm()
        elif opcode == 0xA0: self.__LDY_imm()
        elif opcode == 0x09: self.__ORA_imm()
        elif opcode == 0xE9: self.__SBC_imm()
        #zpg
        elif opcode == 0x65: self.__ADC_zpg()
        elif opcode == 0x25: self.__AND_zpg()
        elif opcode == 0x06: self.__ASL_zpg()
        elif opcode == 0xC5: self.__CMP_zpg()
        elif opcode == 0xE4: self.__CPX_zpg()
        elif opcode == 0xC4: self.__CPY_zpg()
        elif opcode == 0xC6: self.__DEC_zpg()
        elif opcode == 0x45: self.__EOR_zpg()
        elif opcode == 0xE6: self.__INC_zpg()
        elif opcode == 0xA5: self.__LDA_zpg()
        elif opcode == 0xA6: self.__LDX_zpg()
        elif opcode == 0xA4: self.__LDY_zpg()
        elif opcode == 0x46: self.__LSR_zpg()
        elif opcode == 0x05: self.__ORA_zpg()
        elif opcode == 0x26: self.__ROL_zpg()
        elif opcode == 0x66: self.__ROR_zpg()
        elif opcode == 0xE5: self.__SBC_zpg()
        elif opcode == 0x85: self.__STA_zpg()
        elif opcode == 0x86: self.__STX_zpg()
        elif opcode == 0x84: self.__STY_zpg()
        elif opcode == 0x24: self.__BIT_zpg()
        #indirect
        elif opcode == 0x6C: self.__JMP_ind()
        #relative
        elif opcode == 0x90: self.__BCC()
        elif opcode == 0xB0: self.__BCS()
        elif opcode == 0xF0: self.__BEQ()
        elif opcode == 0x30: self.__BMI()
        elif opcode == 0xD0: self.__BNE()
        elif opcode == 0x10: self.__BPL()
        elif opcode == 0x50: self.__BVC()
        elif opcode == 0x70: self.__BVS()
        #absolute
        elif opcode == 0x6D: self.__ADC_abs()
        elif opcode == 0x2D: self.__AND_abs()
        elif opcode == 0x0E: self.__ASL_abs()
        elif opcode == 0x2C: self.__BIT_abs()
        elif opcode == 0xCD: self.__CMP_abs()
        elif opcode == 0xEC: self.__CPX_abs()
        elif opcode == 0xCC: self.__CPY_abs()
        elif opcode == 0xCE: self.__DEC_abs()
        elif opcode == 0x4D: self.__EOR_abs()
        elif opcode == 0xEE: self.__INC_abs()
        elif opcode == 0x4C: self.__JMP_abs()
        elif opcode == 0x20: self.__JSR_abs()
        elif opcode == 0xAD: self.__LDA_abs()
        elif opcode == 0xAE: self.__LDX_abs()
        elif opcode == 0xAC: self.__LDY_abs()
        elif opcode == 0x4E: self.__LSR_abs()
        elif opcode == 0x0D: self.__ORA_abs()
        elif opcode == 0x2E: self.__ROL_abs()
        elif opcode == 0x6E: self.__ROR_abs()
        elif opcode == 0xED: self.__SBC_abs()
        elif opcode == 0x8D: self.__STA_abs()
        elif opcode == 0x8E: self.__STX_abs()
        elif opcode == 0x8C: self.__STY_abs()
        #zpg_x
        elif opcode == 0x75: self.__ADC_zpg_x()
        elif opcode == 0x35: self.__AND_zpg_x()
        elif opcode == 0x16: self.__ASL_zpg_x()
        elif opcode == 0xD5: self.__CMP_zpg_x()
        elif opcode == 0xD6: self.__DEC_zpg_x()
        elif opcode == 0x55: self.__EOR_zpg_x()
        elif opcode == 0xF6: self.__INC_zpg_x()
        elif opcode == 0xB5: self.__LDA_zpg_x()
        elif opcode == 0xB4: self.__LDY_zpg_x()
        elif opcode == 0x56: self.__LSR_zpg_x()
        elif opcode == 0x15: self.__ORA_zpg_x()
        elif opcode == 0x36: self.__ROL_zpg_x()
        elif opcode == 0x76: self.__ROR_zpg_x()
        elif opcode == 0xF5: self.__SBC_zpg_x()
        elif opcode == 0x95: self.__STA_zpg_x()
        elif opcode == 0x94: self.__STY_zpg_x()
        #zpg_y
        elif opcode == 0xB6: self.__LDX_zpg_y()
        elif opcode == 0x96: self.__STX_zpg_y()
        #abs_x
        elif opcode == 0x7D: self.__ADC_abs_x()
        elif opcode == 0x3D: self.__AND_abs_x()
        elif opcode == 0x1E: self.__ASL_abs_x()
        elif opcode == 0xDD: self.__CMP_abs_x()
        elif opcode == 0xDE: self.__DEC_abs_x()
        elif opcode == 0x5D: self.__EOR_abs_x()
        elif opcode == 0xFE: self.__INC_abs_x()
        elif opcode == 0xBD: self.__LDA_abs_x()
        elif opcode == 0xBC: self.__LDY_abs_x()
        elif opcode == 0x5E: self.__LSR_abs_x()
        elif opcode == 0x1D: self.__ORA_abs_x()
        elif opcode == 0x3E: self.__ROL_abs_x()
        elif opcode == 0x7E: self.__ROR_abs_x()
        elif opcode == 0xFD: self.__SBC_abs_x()
        elif opcode == 0x9D: self.__STA_abs_x()
        #abs_y
        elif opcode == 0x79: self.__ADC_abs_y()
        elif opcode == 0x39: self.__AND_abs_y()
        elif opcode == 0xD9: self.__CMP_abs_y()
        elif opcode == 0x59: self.__EOR_abs_y()
        elif opcode == 0xB9: self.__LDA_abs_y()
        elif opcode == 0xBE: self.__LDX_abs_y()
        elif opcode == 0x19: self.__ORA_abs_y()
        elif opcode == 0xF9: self.__SBC_abs_y()
        elif opcode == 0x99: self.__STA_abs_y()
        #x_ind
        elif opcode == 0x61: self.__ADC_x_ind()
        elif opcode == 0x21: self.__AND_x_ind()
        elif opcode == 0xC1: self.__CMP_x_ind()
        elif opcode == 0x41: self.__EOR_x_ind()
        elif opcode == 0xA1: self.__LDA_x_ind()
        elif opcode == 0x01: self.__ORA_x_ind()
        elif opcode == 0xE1: self.__SBC_x_ind()
        elif opcode == 0x81: self.__STA_x_ind()
        #ind_y
        elif opcode == 0x71: self.__ADC_ind_y()
        elif opcode == 0x31: self.__AND_ind_y()
        elif opcode == 0xD1: self.__CMP_ind_y()
        elif opcode == 0x51: self.__EOR_ind_y()
        elif opcode == 0xB1: self.__LDA_ind_y()
        elif opcode == 0x11: self.__ORA_ind_y()
        elif opcode == 0xF1: self.__SBC_ind_y()
        elif opcode == 0x91: self.__STA_ind_y()

        else: self.__addOutput("NA", "none", "NA", "NA")

    ######This is where all of the opcode begins!!######

    #Add Memory to Accumulator with Carry (69)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_imm(self):
        imm = self.__getNextMem()
        res = self.AC + (self.SR & 1) + imm
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, imm, res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "#", byteToString(imm), "--")

    #Add Memory to Accumulator with Carry (65)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_zpg(self):
        zpg = self.__getNextMem()
        res = self.AC + (self.SR&1) + self.__getMem(zpg)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem(zpg), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "zpg", byteToString(zpg), "--")

    #Add Memory to Accumulator with Carry (6D)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        res = self.AC + (self.SR & 1) + self.__getMem((addh<<8) | addl)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((addh<<8) | addl), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "abs", byteToString(addl), byteToString(addh))

    #Add Memory to Accumulator with Carry (75)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_zpg_x(self):
        zpg = self.__getNextMem()
        res = self.AC + (self.SR&1) + self.__getMem((zpg+self.XR)&255)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((zpg+self.XR)&255), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "zpg,x", byteToString(zpg), "--")

    #Add Memory to Accumulator with Carry (7D)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        res = self.AC + (self.SR & 1) + self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((((addh<<8)|addl)+self.XR)&65535), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "abs,x", byteToString(addl), byteToString(addh))

    #Add Memory to Accumulator with Carry (79)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        res = self.AC + (self.SR & 1) + self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((((addh<<8)|addl)+self.YR)&65535), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "abs,y", byteToString(addl), byteToString(addh))

    #Add Memory to Accumulator with Carry (61)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        res = self.AC + (self.SR&1) + self.__getMem((addh<<8)|addl)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((addh<<8)|addl), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "x,ind", byteToString(ind), "--")

    #Add Memory to Accumulator with Carry (71)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ADC_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        res = self.AC + (self.SR&1) + self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        #Check the C and V flags, and trim the result
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((((addh<<8)|addl)+self.YR)&65535), res )
        self.AC = res & 0B11111111
        #Check the Z & N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ADC", "ind,y", byteToString(ind), "--")

    #AND Memory with Accumulator (29)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_imm(self):
        imm = self.__getNextMem()
        self.AC = self.AC & imm
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "#", byteToString(imm), "--")

    #AND Memory with Accumulator (25)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_zpg(self):
        zpg = self.__getNextMem()
        self.AC = self.AC & self.__getMem(zpg)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "zpg", byteToString(zpg), "--")

    #AND Memory with Accumulator (2D)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.__getMem((addh<<8) | addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "abs", byteToString(addl), byteToString(addh))

    #AND Memory with Accumulator (35)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_zpg_x(self):
        zpg = self.__getNextMem()
        self.AC = self.AC & self.__getMem((zpg+self.XR)&255)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "zpg,x", byteToString(zpg), "--")

    #AND Memory with Accumulator (3D)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "abs,x", byteToString(addl), byteToString(addh))

    #AND Memory with Accumulator (39)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "abs,y", byteToString(addl), byteToString(addh))

    #AND Memory with Accumulator (21)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        self.AC = self.AC & self.__getMem((addh<<8)|addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "x,ind", byteToString(ind), "--")

    #AND Memory with Accumulator (31)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __AND_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        self.AC = self.AC & self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("AND", "ind,y", byteToString(ind), "--")

    #ShiftLeft One Bit: Accumulator (0A)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ASL(self):
        self.AC = self.AC << 1
        #Check the C flag
        self.__setC( self.AC )
        #Cut AC down to 8 bits
        self.AC = self.AC & 0B11111111
        #Check the Z and N flags
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ASL", "A", "--", "--")

    #ShiftLeft One Bit: Memory (06)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ASL_zpg(self):
        zpg = self.__getNextMem()
        memory = self.__getMem(zpg) << 1
        self.__setC( memory )
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem(zpg, memory)
        self.__addOutput("ASL", "zpg", byteToString(zpg), "--")

    #ShiftLeft One Bit: Memory (0E)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ASL_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((addh<<8) | addl) << 1
        self.__setC( memory )
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((addh<<8) | addl, memory)
        self.__addOutput("ASL", "abs", byteToString(addl), byteToString(addh))

    #ShiftLeft One Bit: Memory (16)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ASL_zpg_x(self):
        zpg = self.__getNextMem()
        memory = self.__getMem((zpg+self.XR)&255) << 1
        self.__setC( memory )
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((zpg+self.XR)&255, memory)
        self.__addOutput("ASL", "zpg,x", byteToString(zpg), "--")

    #ShiftLeft One Bit: Memory (1E)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ASL_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((((addh<<8)|addl)+self.XR)&65535) << 1
        self.__setC( memory )
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, memory)
        self.__addOutput("ASL", "abs,x", byteToString(addl), byteToString(addh))

    #Branch on Carry Clear (90)
    def __BCC(self):
        rel = self.__getNextMem()
        if self.SR & 1 is 0:
            # we must detect if the rel is a positive or negative number
            if rel & 128 is 0:
                self.PC += rel
            else:
                self.PC -= twosComp(rel)
            #trim the pc
            self.PC = self.PC & 65535
        self.__addOutput("BCC", "rel", byteToString(rel), "--")

    #Branch on Carry Set (B0)
    def __BCS(self):
        rel = self.__getNextMem()
        if self.SR & 1 is not 0:
            # we must detect if the rel is a positive or negative number
            if rel & 128 is 0:
                self.PC += rel
            else:
                self.PC -= twosComp(rel)
            #trim the pc
            self.PC = self.PC & 65535
        self.__addOutput("BCS", "rel", byteToString(rel), "--")

    #Branch on Result Zero (F0)
    def __BEQ(self):
        rel = self.__getNextMem()
        if self.SR & 2 is not 0:
            # we must detect if the rel is a positive or negative number
            if rel & 128 is 0:
                self.PC += rel
            else:
                self.PC -= twosComp(rel)
            #trim the pc
            self.PC = self.PC & 65535
        self.__addOutput("BEQ", "rel", byteToString(rel), "--")

    #Test Bits in Memory with Accumulator (24)
    # N - Set to M7
    # V - Set to M6
    # Z - Set if A and M  is Zero
    def __BIT_zpg(self):
        zpg = self.__getNextMem()
        self.__setZ(self.__getMem(zpg) & self.AC)
        self.SR = self.SR & 0b111111
        self.SR = self.SR | (self.__getMem(zpg) & 0b11000000)
        self.__addOutput("BIT", "zpg", byteToString(zpg), "--")

    #Test Bits in Memory with Accumulator (2C)
    # N - Set to M7
    # V - Set to M6
    # Z - Set if A and M  is Zero
    def __BIT_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setZ(self.__getMem((addh<<8) | addl) & self.AC)
        self.SR = self.SR & 0b111111
        self.SR = self.SR | (self.__getMem((addh<<8) | addl) & 0b11000000)
        self.__addOutput("BIT", "abs", byteToString(addl), byteToString(addh))

    #Branch on Result Minus (30)
    def __BMI(self):
        rel = self.__getNextMem()
        if self.SR & 128 is not 0:
            # we must detect if the rel is a positive or negative number
            if rel & 128 is 0:
                self.PC += rel
            else:
                self.PC -= twosComp(rel)
            #trim the pc
            self.PC = self.PC & 65535
        self.__addOutput("BMI", "rel", byteToString(rel), "--")

    #Branch on Result not Zero (D0)
    def __BNE(self):
        rel = self.__getNextMem()
        if self.SR & 2 is 0:
            # we must detect if the rel is a positive or negative number
            if rel & 128 is 0:
                self.PC += rel
            else:
                self.PC -= twosComp(rel)
            #trim the pc
            self.PC = self.PC & 65535
        self.__addOutput("BNE", "rel", byteToString(rel), "--")

    #Branch on Result Plus (10)
    def __BPL(self):
        rel = self.__getNextMem()
        if self.SR & 128 is 0:
            # we must detect if the rel is a positive or negative number
            if rel & 128 is 0:
                self.PC += rel
            else:
                self.PC -= twosComp(rel)
            #trim the pc
            self.PC = self.PC & 65535
        self.__addOutput("BPL", "rel", byteToString(rel), "--")

    #Branch on Result Plus (50)
    def __BVC(self):
        rel = self.__getNextMem()
        self.__addOutput("BVC", "rel", byteToString(rel), "--")

    #Branch on Result Plus (70)
    def __BVS(self):
        rel = self.__getNextMem()
        self.__addOutput("BVS", "rel", byteToString(rel), "--")

    #Force Break (00)
    # I - Set
    # B - Set
    def __BRK(self):
        #Set the B and I flags
        self.SR = self.SR | 0B00010000
        self.SR = self.SR | 0B00000100
        #Push PC and SR onto the stack
        self.__pushOnStack(self.PC+1 >> 8)
        self.__pushOnStack(self.PC+1 & 0B11111111)
        self.__pushOnStack(self.SR)
        self.__addOutput("BRK", "impl", "--", "--")

    #Clear Carry Flag (18)
    # C - Clear
    def __CLC(self):
        self.SR = self.SR & 0B11111110
        self.__addOutput("CLC", "impl", "--", "--")

    #Clear Decimal Mode (D8)
    # D - Clear
    def __CLD(self):
        self.SR = self.SR & 0B11110111
        self.__addOutput("CLD", "impl", "--", "--")

    #Clear Interrupt Disable Bit (58)
    # I - Clear
    def __CLI(self):
        self.SR = self.SR & 0B11111011
        self.__addOutput("CLI", "impl", "--", "--")

    #Clear Overflow Flag (B8)
    # V - Clear
    def __CLV(self):
        self.SR = self.SR & 0B10111111
        self.__addOutput("CLV", "impl", "--", "--")

    #Compare Immediate with Accumulator (C9)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_imm(self):
        imm = self.__getNextMem()
        self.__setC( self.AC + twosComp(imm))
        self.__setN( self.AC + twosComp(imm))
        self.__setZ( (self.AC + twosComp(imm)) & 0B11111111 )
        self.__addOutput("CMP", "#", byteToString(imm), "--")

    #Compare Memory with Accumulator (C5)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_zpg(self):
        zpg = self.__getNextMem()
        self.__setC( self.AC + twosComp(self.__getMem(zpg)) )
        self.__setN( self.AC + twosComp(self.__getMem(zpg)) )
        self.__setZ( self.AC + twosComp(self.__getMem(zpg)) & 0B11111111 )
        self.__addOutput("CMP", "zpg", byteToString(zpg), "--")

    #Compare Memory with Accumulator (CD)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setC( self.AC + twosComp(self.__getMem((addh<<8) | addl)) )
        self.__setN( self.AC + twosComp(self.__getMem((addh<<8) | addl)) )
        self.__setZ( self.AC + twosComp(self.__getMem((addh<<8) | addl)) & 0B11111111 )
        self.__addOutput("CMP", "abs", byteToString(addl), byteToString(addh))

    #Compare Memory with Accumulator (D5)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_zpg_x(self):
        zpg = self.__getNextMem()
        self.__setC( self.AC + twosComp(self.__getMem((zpg+self.XR)&255)) )
        self.__setN( self.AC + twosComp(self.__getMem((zpg+self.XR)&255)) )
        self.__setZ( self.AC + twosComp(self.__getMem((zpg+self.XR)&255)) & 255 )
        self.__addOutput("CMP", "zpg,x", byteToString(zpg), "--")

    #Compare Memory with Accumulator (DD)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setC( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.XR)&65535)) )
        self.__setN( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.XR)&65535)) )
        self.__setZ( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.XR)&65535)) & 0B11111111 )
        self.__addOutput("CMP", "abs,x", byteToString(addl), byteToString(addh))

    #Compare Memory with Accumulator (D9)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setC( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) )
        self.__setN( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) )
        self.__setZ( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) & 0B11111111 )
        self.__addOutput("CMP", "abs,y", byteToString(addl), byteToString(addh))

    #Compare Memory with Accumulator (C1)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        self.__setC( self.AC + twosComp(self.__getMem((addh<<8)|addl)) )
        self.__setN( self.AC + twosComp(self.__getMem((addh<<8)|addl)) )
        self.__setZ( self.AC + twosComp(self.__getMem((addh<<8)|addl)) & 255 )
        self.__addOutput("CMP", "x,ind", byteToString(ind), "--")

    #Compare Memory with Accumulator (D1)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CMP_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        self.__setC( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) )
        self.__setN( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) )
        self.__setZ( self.AC + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) & 255 )
        self.__addOutput("CMP", "ind,y", byteToString(ind), "--")

    #Compare Immediate and Index X (E0)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CPX_imm(self):
        imm = self.__getNextMem()
        self.__setC( self.XR + twosComp(imm) )
        self.__setN( self.XR + twosComp(imm) )
        self.__setZ( self.XR + twosComp(imm) & 0B11111111 )
        self.__addOutput("CPX", "#", byteToString(imm), "--")

    #Compare Memory and Index X (E4)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CPX_zpg(self):
        zpg = self.__getNextMem()
        self.__setC( self.XR + twosComp(self.__getMem(zpg)) )
        self.__setN( self.XR + twosComp(self.__getMem(zpg)) )
        self.__setZ( self.XR + twosComp(self.__getMem(zpg)) & 0B11111111 )
        self.__addOutput("CPX", "zpg", byteToString(zpg), "--")

    #Compare Memory and Index X (EC)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CPX_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setC( self.XR + twosComp(self.__getMem((addh<<8) | addl)) )
        self.__setN( self.XR + twosComp(self.__getMem((addh<<8) | addl)) )
        self.__setZ( self.XR + twosComp(self.__getMem((addh<<8) | addl)) & 0B11111111 )
        self.__addOutput("CPX", "abs", byteToString(addl), byteToString(addh))

    #Compare Immediate and Index Y (C0)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CPY_imm(self):
        imm = self.__getNextMem()
        self.__setC( self.YR + twosComp(imm) )
        self.__setN( self.YR + twosComp(imm) )
        self.__setZ( self.YR + twosComp(imm) & 0B11111111 )
        self.__addOutput("CPY", "#", byteToString(imm), "--")

    #Compare Memory and Index Y (C4)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CPY_zpg(self):
        zpg = self.__getNextMem()
        self.__setC( self.YR + twosComp(self.__getMem(zpg)) )
        self.__setN( self.YR + twosComp(self.__getMem(zpg)) )
        self.__setZ( self.YR + twosComp(self.__getMem(zpg)) & 0B11111111 )
        self.__addOutput("CPY", "zpg", byteToString(zpg), "--")

    #Compare Memory and Index Y (CC)
    # C - Set if adition had a carry or subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __CPY_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setC( self.YR + twosComp(self.__getMem((addh<<8) | addl)) )
        self.__setN( self.YR + twosComp(self.__getMem((addh<<8) | addl)) )
        self.__setZ( self.YR + twosComp(self.__getMem((addh<<8) | addl)) & 0B11111111 )
        self.__addOutput("CPY", "abs", byteToString(addl), byteToString(addh))

    #Decrement Memory by One (C6)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __DEC_zpg(self):
        zpg = self.__getNextMem()
        memory = self.__getMem(zpg) + twosComp(1)
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem(zpg, memory)
        self.__addOutput("DEC", "zpg", byteToString(zpg), "--")

    #Decrement Memory by One (CE)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __DEC_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((addh<<8) | addl) + twosComp(1)
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((addh<<8) | addl, memory)
        self.__addOutput("DEC", "abs", byteToString(addl), byteToString(addh))

    #Decrement Memory by One (D6)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __DEC_zpg_x(self):
        zpg = self.__getNextMem()
        memory = self.__getMem((zpg+self.XR)&255) + twosComp(1)
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((zpg+self.XR)&255, memory)
        self.__addOutput("DEC", "zpg,x", byteToString(zpg), "--")

    #Decrement Memory by One (DE)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __DEC_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((((addh<<8)|addl)+self.XR)&65535) + twosComp(1)
        memory = memory & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, memory)
        self.__addOutput("DEC", "abs,x", byteToString(addl), byteToString(addh))

    #Decrement Index X by One (CA)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __DEX(self):
        #subtract 1 and cut XR down to 8 bits
        self.XR = (self.XR-1) & 0B11111111
        #check for Z and N flags
        self.__setZ( self.XR )
        self.__setN( self.XR )
        self.__addOutput("DEX", "impl", "--", "--")

    #Decrement Index Y by One (88)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __DEY(self):
        #subtract 1 and cut YR down to 8 bits
        self.YR = (self.YR-1) & 0B11111111
        #check for Z and N flags
        self.__setZ( self.YR )
        self.__setN( self.YR )
        self.__addOutput("DEY", "impl", "--", "--")

    #Exclusive-OR Immediate with Accumulator (49)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_imm(self):
        imm = self.__getNextMem()
        self.AC = self.AC ^ imm
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "#", byteToString(imm), "--")

    #Exclusive-OR Memory with Accumulator (45)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_zpg(self):
        zpg = self.__getNextMem()
        self.AC = self.AC ^ self.__getMem(zpg)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "zpg", byteToString(zpg), "--")

    #Exclusive-OR Memory with Accumulator (4D)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.AC ^ self.__getMem((addh<<8) | addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "abs", byteToString(addl), byteToString(addh))

    #Exclusive-OR Memory with Accumulator ()
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_zpg_x(self):
        zpg = self.__getNextMem()
        self.AC = self.AC ^ self.__getMem((zpg+self.XR)&255)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "zpg,x", byteToString(zpg), "--")

    #Exclusive-OR Memory with Accumulator ()
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.AC ^ self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "abs,x", byteToString(addl), byteToString(addh))

    #Exclusive-OR Memory with Accumulator ()
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.AC ^ self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "abs,y", byteToString(addl), byteToString(addh))

    #Exclusive-OR Memory with Accumulator ()
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        self.AC = self.AC ^ self.__getMem((addh<<8)|addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "x,ind", byteToString(ind), "--")

    #Exclusive-OR Memory with Accumulator ()
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __EOR_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        self.AC = self.AC ^ self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("EOR", "ind,y", byteToString(ind), "--")

    #Increment Memory by One (E6)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __INC_zpg(self):
        zpg = self.__getNextMem()
        memory = (self.__getMem(zpg)+1) & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem(zpg, memory)
        self.__addOutput("INC", "zpg", byteToString(zpg), "--")

    #Increment Memory by One (EE)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __INC_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = (self.__getMem((addh<<8) | addl)+1) & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((addh<<8) | addl, memory)
        self.__addOutput("INC", "abs", byteToString(addl), byteToString(addh))

    #Increment Memory by One (F6)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __INC_zpg_x(self):
        zpg = self.__getNextMem()
        memory = (self.__getMem((zpg+self.XR)&255)+1) & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((zpg+self.XR)&255, memory)
        self.__addOutput("INC", "zpg,x", byteToString(zpg), "--")

    #Increment Memory by One (FE)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __INC_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = (self.__getMem((((addh<<8)|addl)+self.XR)&65535)+1) & 0B11111111
        self.__setN( memory )
        self.__setZ( memory )
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, memory)
        self.__addOutput("INC", "abs,x", byteToString(addl), byteToString(addh))

    #Increment Index X by One (E8)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __INX(self):
        self.XR = (self.XR+1) & 0B11111111
        #check for N and Z flags
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("INX", "impl", "--", "--")

    #Increment Index Y by One (C8)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __INY(self):
        self.YR = (self.YR+1) & 0B11111111
        #check for N and Z flag
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("INY", "impl", "--", "--")

    #Jump to New Location (6C)
    def __JMP_ind(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.PC = self.__getMem((addh<<8) | addl)
        self.PC = self.PC | ( self.__getMem(((addh<<8) | addl) +1) << 8 )
        self.__addOutput("JMP", "ind", byteToString(addl), byteToString(addh))

    #Jump to New Location (46)
    def __JMP_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.PC = (addh<<8) | addl
        self.__addOutput("JMP", "abs", byteToString(addl), byteToString(addh))

    #Jump to New Location Saving Return Address (20)
    def __JSR_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        #push high bits of PC
        self.__pushOnStack(self.PC-1 >> 8)
        #push low bits of PC
        self.__pushOnStack(self.PC-1 & 0B11111111)
        self.PC = (addh<<8) | addl
        self.__addOutput("JSR", "abs", byteToString(addl), byteToString(addh))

    #Load Accumulator with Immediate (A9)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_imm(self):
        self.AC = self.__getNextMem()
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "#", byteToString(self.AC), "--")

    #Load Accumulator with Memory (A5)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_zpg(self):
        zpg = self.__getNextMem()
        self.AC = self.__getMem(zpg)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "zpg", byteToString(zpg), "--")

    #Load Accumulator with Memory (AD)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.__getMem((addh<<8) | addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "abs", byteToString(addl), byteToString(addh))

    #Load Accumulator with Memory (B5)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_zpg_x(self):
        zpg = self.__getNextMem()
        self.AC = self.__getMem((zpg+self.XR)&255)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "zpg,x", byteToString(zpg), "--")

    #Load Accumulator with Memory (BD)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "abs,x", byteToString(addl), byteToString(addh))

    #Load Accumulator with Memory (B9)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "abs,y", byteToString(addl), byteToString(addh))

    #Load Accumulator with Memory (A1)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        self.AC = self.__getMem((addh<<8)|addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "x,ind", byteToString(ind), "--")

    #Load Accumulator with Memory (B1)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDA_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        self.AC = self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("LDA", "ind,y", byteToString(ind), "--")

    #Load Index X with Immediate (A2)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDX_imm(self):
        self.XR = self.__getNextMem()
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("LDX", "#", byteToString(self.XR), "--")

    #Load Index X with Memory (A6)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDX_zpg(self):
        zpg = self.__getNextMem()
        self.XR = self.__getMem(zpg)
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("LDX", "zpg", byteToString(zpg), "--")

    #Load Index X with Memory (AE)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDX_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.XR = self.__getMem((addh<<8) | addl)
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("LDX", "abs", byteToString(addl), byteToString(addh))

    #Load Index X with Memory (B6)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDX_zpg_y(self):
        zpg = self.__getNextMem()
        self.XR = self.__getMem((zpg+self.YR)&255)
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("LDX", "zpg,y", byteToString(zpg), "--")

    #Load Index X with Memory (BE)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDX_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.XR = self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("LDX", "abs,y", byteToString(addl), byteToString(addh))

    #Load Index Y with Immediate (A0)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDY_imm(self):
        self.YR = self.__getNextMem()
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("LDY", "#", byteToString(self.YR), "--")

    #Load Index Y with Memory (A4)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDY_zpg(self):
        zpg = self.__getNextMem()
        self.YR = self.__getMem(zpg)
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("LDY", "zpg", byteToString(zpg), "--")

    #Load Index Y with Memory (AC)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDY_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.YR = self.__getMem((addh<<8) | addl)
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("LDY", "abs", byteToString(addl), byteToString(addh))

    #Load Index Y with Memory (B4)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDY_zpg_x(self):
        zpg = self.__getNextMem()
        self.YR = self.__getMem((zpg+self.XR)&255)
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("LDY", "zpg,x", byteToString(zpg), "--")

    #Load Index Y with Memory (BC)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __LDY_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.YR = self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("LDY", "abs,x", byteToString(addl), byteToString(addh))

    #Shift One BitRight: Accumulator (4A)
    # C - Set if we are pushing a bit out
    # Z - Set if result is Zero
    def __LSR(self):
        #check for the C flag
        self.__setC( self.AC << 8 )
        self.AC = self.AC >> 1
        #check for Z flag
        self.__setZ( self.AC )
        self.__addOutput("LSR", "A", "--", "--")

    #Shift One BitRight: Memory (46)
    # C - Set if we are pushing a bit out
    # Z - Set if result is Zero
    def __LSR_zpg(self):
        zpg = self.__getNextMem()
        memory = self.__getMem(zpg)
        self.__setC( memory << 8 )
        memory = memory >> 1
        self.__setZ( memory )
        self.__setMem(zpg, memory)
        self.__addOutput("LSR", "zpg", byteToString(zpg), "--")

    #Shift One BitRight: Memory (4E)
    # C - Set if we are pushing a bit out
    # Z - Set if result is Zero
    def __LSR_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((addh<<8) | addl)
        self.__setC( memory << 8 )
        memory = memory >> 1
        self.__setZ( memory )
        self.__setMem((addh<<8) | addl, memory)
        self.__addOutput("LSR", "abs", byteToString(addl), byteToString(addh))

    #Shift One BitRight: Memory (56)
    # C - Set if we are pushing a bit out
    # Z - Set if result is Zero
    def __LSR_zpg_x(self):
        zpg = self.__getNextMem()
        memory = self.__getMem((zpg+self.XR)&255)
        self.__setC( memory << 8 )
        memory = memory >> 1
        self.__setZ( memory )
        self.__setMem((zpg+self.XR)&255, memory)
        self.__addOutput("LSR", "zpg,x", byteToString(zpg), "--")

    #Shift One BitRight: Memory (5E)
    # C - Set if we are pushing a bit out
    # Z - Set if result is Zero
    def __LSR_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        self.__setC( memory << 8 )
        memory = memory >> 1
        self.__setZ( memory )
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, memory)
        self.__addOutput("LSR", "abs,x", byteToString(addl), byteToString(addh))

    #No Operation (EA)
    def __NOP(self):
        self.__addOutput("NOP", "impl", "--", "--")

    #OR Memory with Accumulator (09)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_imm(self):
        imm = self.__getNextMem()
        self.AC = self.AC | imm
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "#", byteToString(imm), "--")

    #OR Memory with Accumulator (05)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_zpg(self):
        zpg = self.__getNextMem()
        self.AC = self.AC | self.__getMem(zpg)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "zpg", byteToString(zpg), "--")

    #OR Memory with Accumulator (0D)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.AC | self.__getMem((addh<<8) | addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "abs", byteToString(addl), byteToString(addh))

    #OR Memory with Accumulator (15)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_zpg_x(self):
        zpg = self.__getNextMem()
        self.AC = self.AC | self.__getMem((zpg+self.XR)&255)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "zpg,x", byteToString(zpg), "--")

    #OR Memory with Accumulator (1D)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.AC | self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "abs,x", byteToString(addl), byteToString(addh))

    #OR Memory with Accumulator (19)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.AC = self.AC | self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "abs,y", byteToString(addl), byteToString(addh))

    #OR Memory with Accumulator (01)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        self.AC = self.AC | self.__getMem((addh<<8)|addl)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "x,ind", byteToString(ind), "--")

    #OR Memory with Accumulator (11)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ORA_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        self.AC = self.AC | self.__getMem((((addh<<8)|addl)+self.YR)&65535)
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("ORA", "ind,y", byteToString(ind), "--")

    #Push Accumulator on Stack (48)
    def __PHA(self):
        self.__pushOnStack(self.AC)
        self.__addOutput("PHA", "impl", "--", "--")

    #Push Processor Status on Stack (08)
    def __PHP(self):
        self.__pushOnStack(self.SR)
        self.__addOutput("PHP", "impl", "--", "--")

    #Pull Accumulator from Stack (68)
    def __PLA(self):
        self.AC = self.__pullFromStack()
        self.__addOutput("PLA", "impl", "--", "--")

    #PullProcessor Status from Stack (28)
    def __PLP(self):
        self.SR = self.__pullFromStack() | 0B00100000
        self.__addOutput("PLP", "impl", "--", "--")

    #Rotate One Bit Left: Accumulator (2A)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROL(self):
        #get the carry bit
        carry = self.AC >> 7
        self.AC = (self.AC << 1) & 0B11111111
        self.AC = self.AC | (self.SR & 1)
        #Check N, C, & Z flags
        self.__setN( self.AC )
        self.__setC( carry << 8 )
        self.__setZ( self.AC )
        self.__addOutput("ROL", "A", "--", "--")

    #Rotate One Bit Left: Accumulator (05)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROL_zpg(self):
        zpg = self.__getNextMem()
        memory = self.__getMem(zpg)
        carry = memory >> 7
        memory = (memory << 1) & 0B11111111
        memory = memory | (self.SR & 1)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem(zpg, memory)
        self.__addOutput("ROL", "zpg", byteToString(zpg), "--")

    #Rotate One Bit Left: Accumulator (2E)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROL_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((addh<<8) | addl)
        carry = memory >> 7
        memory = (memory << 1) & 0B11111111
        memory = memory | (self.SR & 1)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem((addh<<8) | addl, memory)
        self.__addOutput("ROL", "abs", byteToString(addl), byteToString(addh))

    #Rotate One Bit Left: Accumulator (36)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROL_zpg_x(self):
        zpg = self.__getNextMem()
        memory = self.__getMem((zpg+self.XR)&255)
        carry = memory >> 7
        memory = (memory << 1) & 0B11111111
        memory = memory | (self.SR & 1)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem((zpg+self.XR)&255, memory)
        self.__addOutput("ROL", "zpg,x", byteToString(zpg), "--")

    #Rotate One Bit Left: Accumulator (3E)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROL_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        carry = memory >> 7
        memory = (memory << 1) & 0B11111111
        memory = memory | (self.SR & 1)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, memory)
        self.__addOutput("ROL", "abs,x", byteToString(addl), byteToString(addh))

    #RotateOne Bit Right: Accumulator (6A)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROR(self):
        #Get the carry bit
        carry = self.AC & 0B00000001
        #shift, move the carry bit in, and trim
        self.AC = ( self.AC >> 1 ) & 0B11111111
        self.AC = self.AC | ((self.SR & 1) << 7)
        self.__setN( self.AC )
        self.__setC( carry << 8 )
        self.__setZ( self.AC )
        self.__addOutput("ROR", "A", "--", "--")

    #RotateOne Bit Right: Accumulator (66)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROR_zpg(self):
        zpg = self.__getNextMem()
        memory = self.__getMem(zpg)
        carry = memory & 0B00000001
        memory = ( memory >> 1 ) & 0B11111111
        memory = memory | ((self.SR & 1) << 7)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem(zpg, memory)
        self.__addOutput("ROR", "zpg", byteToString(zpg), "--")

    #RotateOne Bit Right: Accumulator (6E)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROR_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((addh<<8) | addl)
        carry = memory & 0B00000001
        memory = ( memory >> 1 ) & 0B11111111
        memory = memory | ((self.SR & 1) << 7)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem((addh<<8) | addl, memory)
        self.__addOutput("ROR", "abs", byteToString(addl), byteToString(addh))

    #RotateOne Bit Right: Accumulator (76)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROR_zpg_x(self):
        zpg = self.__getNextMem()
        memory = self.__getMem((zpg+self.XR)&255)
        carry = memory & 0B00000001
        memory = ( memory >> 1 ) & 0B11111111
        memory = memory | ((self.SR & 1) << 7)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem((zpg+self.XR)&255, memory)
        self.__addOutput("ROR", "zpg,x", byteToString(zpg), "--")

    #RotateOne Bit Right: Accumulator (7E)
    # C - Set if a bit is being pushed out
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __ROR_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        memory = self.__getMem((((addh<<8)|addl)+self.XR)&65535)
        carry = memory & 0B00000001
        memory = ( memory >> 1 ) & 0B11111111
        memory = memory | ((self.SR & 1) << 7)
        self.__setN( memory )
        self.__setC( carry << 8 )
        self.__setZ( memory )
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, memory)
        self.__addOutput("ROR", "abs,x", byteToString(addl), byteToString(addh))

    #Return from Interrupt (40)
    def __RTI(self):
        self.SR = __pullFromStack();
        self.PC = __pullFromStack();
        self.PC = self.PC | (__pullFromStack() << 8)
        self.__addOutput("RTI", "impl", "--", "--")

    #Return from Subroutine (60)
    def __RTS(self):
        self.PC = self.__pullFromStack();
        self.PC = self.PC | (self.__pullFromStack() << 8)
        self.PC += 1;
        self.__addOutput("RTS", "impl", "--", "--")

    #Subtract Memory from Accumulator with Borrow (E9)
    # V - Set if math operatrion ended up with wrong sign bit
    # C - Set if the subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_imm(self):
        imm = self.__getNextMem()
        res = self.AC + ( twosComp(self.SR & 1) + twosComp(imm) ) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, imm, res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "#", byteToString(imm), "--")

    #Subtract Memory from Accumulator with Borrow (E5)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_zpg(self):
        zpg = self.__getNextMem()
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem(zpg)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem(zpg), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "zpg", byteToString(zpg), "--")

    #Subtract Memory from Accumulator with Borrow (ED)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem((addh<<8) | addl)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((addh<<8) | addl), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "abs", byteToString(addl), byteToString(addh))

    #Subtract Memory from Accumulator with Borrow (F5)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_zpg_x(self):
        zpg = self.__getNextMem()
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem((zpg+self.XR)&255)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((zpg+self.XR)&255), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "zpg,x", byteToString(zpg), "--")

    #Subtract Memory from Accumulator with Borrow (FD)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem((((addh<<8)|addl)+self.XR)&65535)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((((addh<<8)|addl)+self.XR)&65535), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "abs,x", byteToString(addl), byteToString(addh))

    #Subtract Memory from Accumulator with Borrow (F9)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((((addh<<8)|addl)+self.YR)&65535), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "abs,y", byteToString(addl), byteToString(addh))

    #Subtract Memory from Accumulator with Borrow (E1)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem((addh<<8)|addl)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((addh<<8)|addl), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "x,ind", byteToString(ind), "--")

    #Subtract Memory from Accumulator with Borrow (F1)
    # V - Set if subtraction ended up with wrong sign bit
    # C - Set if subtraction had a borrow
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __SBC_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        res = self.AC + twosComp(self.SR & 1) + twosComp(self.__getMem((((addh<<8)|addl)+self.YR)&65535)) & 0B11111111
        self.__setC( res )
        self.__setV( self.AC, self.__getMem((((addh<<8)|addl)+self.YR)&65535), res )
        self.__setN( res )
        self.__setZ( res )
        self.AC = res
        self.__addOutput("SBC", "ind,y", byteToString(ind), "--")

    #Set Carry Flag (38)
    # C - Set
    def __SEC(self):
        self.SR = self.SR | 0B00000001
        self.__addOutput("SEC", "impl", "--", "--")

    #Set Decimal Flag (F8)
    # D - Set
    def __SED(self):
        self.SR = self.SR | 0B00001000
        self.__addOutput("SED", "impl", "--", "--")

    #Set Interrupt Disable Status (78)
    # I - Set
    def __SEI(self):
        self.SR = self.SR | 0B00000100
        self.__addOutput("SEI", "impl", "--", "--")

    #Store Accumulator in Memory (85)
    def __STA_zpg(self):
        zpg = self.__getNextMem()
        self.__setMem(zpg, self.AC)
        self.__addOutput("STA", "zpg", byteToString(zpg), "--")

    #Store Accumulator in Memory (8D)
    def __STA_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setMem((addh<<8) | addl, self.AC)
        self.__addOutput("STA", "abs", byteToString(addl), byteToString(addh))

    #Store Accumulator in Memory (95)
    def __STA_zpg_x(self):
        zpg = self.__getNextMem()
        self.__setMem((zpg+self.XR)&255, self.AC)
        self.__addOutput("STA", "zpg,x", byteToString(zpg), "--")

    #Store Accumulator in Memory (9D)
    def __STA_abs_x(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setMem((((addh<<8)|addl)+self.XR)&65535, self.AC)
        self.__addOutput("STA", "abs,x", byteToString(addl), byteToString(addh))

    #Store Accumulator in Memory (99)
    def __STA_abs_y(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setMem((((addh<<8)|addl)+self.YR)&65535, self.AC)
        self.__addOutput("STA", "abs,y", byteToString(addl), byteToString(addh))

    #Store Accumulator in Memory (81)
    def __STA_x_ind(self):
        ind = self.__getNextMem()
        addl = self.__getMem((ind+self.XR)&255)
        addh = self.__getMem(((ind+self.XR)&255)+1)
        self.__setMem((addh<<8)|addl, self.AC)
        self.__addOutput("STA", "x,ind", byteToString(ind), "--")

    #Store Accumulator in Memory (91)
    def __STA_ind_y(self):
        ind = self.__getNextMem()
        addl = self.__getMem(ind)
        addh = self.__getMem(ind+1)
        self.__addOutput("STA", "ind,y", byteToString(ind), "--")

    #Store Index X in Memory (86)
    def __STX_zpg(self):
        zpg = self.__getNextMem()
        self.__setMem(zpg, self.XR)
        self.__addOutput("STX", "zpg", byteToString(zpg), "--")

    #Store Index X in Memory (8E)
    def __STX_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setMem((addh<<8) | addl, self.XR)
        self.__addOutput("STX", "abs", byteToString(addl), byteToString(addh))

    #Store Index X in Memory (96)
    def __STX_zpg_y(self):
        zpg = self.__getNextMem()
        self.__setMem((zpg+self.YR)&255, self.XR)
        self.__addOutput("STX", "zpg,y", byteToString(zpg), "--")

    #SoreIndex Y in Memory (84)
    def __STY_zpg(self):
        zpg = self.__getNextMem()
        self.__setMem(zpg, self.YR)
        self.__addOutput("STY", "zpg", byteToString(zpg), "--")

    #SoreIndex Y in Memory (8C)
    def __STY_abs(self):
        addl = self.__getNextMem()
        addh = self.__getNextMem()
        self.__setMem((addh<<8) | addl, self.YR)
        self.__addOutput("STY", "abs", byteToString(addl), byteToString(addh))

    #SoreIndex Y in Memory (84)
    def __STY_zpg_x(self):
        zpg = self.__getNextMem()
        self.__setMem((zpg+self.XR)&255, self.YR)
        self.__addOutput("STY", "zpg,x", byteToString(zpg), "--")

    #Transfer Accumulator to Index X (AA)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __TAX(self):
        self.XR = self.AC
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("TAX", "impl", "--", "--")

    #Transfer Accumulator to Index Y (A8)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __TAY(self):
        self.YR = self.AC
        self.__setN( self.YR )
        self.__setZ( self.YR )
        self.__addOutput("TAY", "impl", "--", "--")

    #Transfer Stack Pointer to Index X (BA)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __TSX(self):
        self.XR = self.SP
        self.__setN( self.XR )
        self.__setZ( self.XR )
        self.__addOutput("TSX", "impl", "--", "--")

    #Transfer Index X to Accumulator (8A)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __TXA(self):
        self.AC = self.XR
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("TXA", "impl", "--", "--")

    #Transfer Index X to Stack Pointer (9A)
    def __TXS(self):
        self.SP = self.XR
        self.__addOutput("TXS", "impl", "--", "--")

    #Transfer Index Y to Accumulator (98)
    # N - Set if result in Negative
    # Z - Set if result is Zero
    def __TYA(self):
        self.AC = self.YR
        self.__setN( self.AC )
        self.__setZ( self.AC )
        self.__addOutput("TYA", "impl", "--", "--")
