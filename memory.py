#This is the Memory object for the T34, it will keep track of all the
#memory in the program
class Memory:
    #declare memory with the number of bytes you need
    def __init__(self, numOfBytes):
        #convert to an int if numOfBytes is a string
        if isinstance(numOfBytes, str):
            numOfBytes = int( numOfBytes, 16 )
        self.the_memory = bytearray(numOfBytes)

    #use this to return a chunk of memory
    def returnChunk(self, begIndex, endIndex):
        #convert the hex input to ints
        if isinstance(begIndex, str):
            begIndex = int( begIndex, 16 )
        if isinstance(endIndex, str):
            endIndex = int( endIndex, 16 )
        #return a bytearray containing the data
        return self.the_memory[ begIndex : endIndex+1 ]

    #use this to get a single memory address
    def returnSingle(self, index):
        #convert the hex input to an int
        if isinstance(index, str):
            index = int( index, 16 )
        #return the data
        return self.the_memory[ index ]

    #use this to set a chunk of memory
    def setChunk(self, begIndex, memChunkData):
        #Convert the hex input to int
        if isinstance(begIndex, str):
            begIndex = int( begIndex, 16 )
        #Convert the string data to hex format
        if isinstance(memChunkData, str):
            memChunkData = bytearray.fromhex( memChunkData )
        #cycle through and element and set it in the memory
        for i in memChunkData:
            self.setMem(begIndex, i)
            begIndex = begIndex + 1

    def setMem(self, address, data):
        #Convert the hex input to int
        if isinstance(address, str):
            address = int( address, 16 )
        if isinstance(data, str):
            data = int( data, 16 )

        self.the_memory[address] = data
