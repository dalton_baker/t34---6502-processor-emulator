#!/usr/bin/env python3
from monitor import Monitor
from memory import Memory
from fileProcessing import processFile
from cpu import CPU
import signal
import sys

#This is here to handle CTRL+C being pressed
def handleExit(signum, frame):
    print()
    exit(0)
signal.signal(signal.SIGINT, handleExit)


def main():
    #create the memory and the monitor
    T34_memory = Memory("10000")
    T34_monitor = Monitor()
    T34_CPU = CPU(T34_memory)

    #if a file name was provided, try to open and process it
    if len(sys.argv) > 1:
        for data in processFile(sys.argv[1]):
            T34_memory.setChunk(data[0], data[1])

    while True:
        inputData = T34_monitor.getInput()

        if "PrintGroup" in inputData:
            memoryData = T34_memory.returnChunk( inputData[1], inputData[2] )
            T34_monitor.printChunk(inputData[1], inputData[2], memoryData)

        elif "SetChunk" in inputData:
            T34_memory.setChunk(inputData[1], inputData[2])

        elif "RunProg" in inputData:
            T34_CPU.runProg(inputData[1])

        elif "PrintSingle" in inputData:
            memoryData = T34_memory.returnSingle(inputData[1])
            T34_monitor.printSingle(inputData[1], memoryData)

        else:
            print(" INPUT ERROR: Input not recognized")

main()
