def byteToString(byte):
    hexOut = ""

    if byte < 16:
        hexOut = hexOut + "0"

    hexOut = hexOut + str( hex( byte ) )[2:].upper()

    return hexOut

#this will convert a bytearray to a string, for printing
def bytearrayToString(bytearrayData):
    #declair the hex output string
    hexOut = ""

    #cycle through the bytearray and insert each element into the string
    for i in bytearrayData:
        #Otherwise convert the byte into hex and add to the string
        hexOut = hexOut + byteToString(i) + ' '

    #print the output
    return hexOut

#This is the monitor object, it will handle all of the input and output
#of the T34 machine
class Monitor:
    #this will take inpout and return it to the T34
    def getInput(self):
        try:
            #display the monitor prompt
            inputData = input("> ")

            #if "exit" is typed, exit the program
            if inputData == "exit":
                exit(0)

            #return whatever is typed
            return self.__decodeInputData(inputData)

        #This will handle a CTRL+D to end the program
        except EOFError:
            print()
            exit(0)

    def printSingle(self, byteAddress, bytearrayData):
        output = " " + "{: <6}".format(byteAddress).upper()
        output = output + byteToString(bytearrayData)
        print(output)
        return

    def printChunk(self, begAddress, endAddress, bytearrayData):
        #get the offset and the actual begining address
        offset = int(begAddress, 16) % 8;
        begAddress = int(begAddress, 16) - offset
        endAddress = int(endAddress, 16)

        #create the first line of the output with the offset
        outputStr = " " + "{: <6}".format(byteToString(begAddress))
        outputStr = outputStr + "{: <{empty}}".format("", empty=offset*3)
        outputStr = outputStr + bytearrayToString(bytearrayData[:8-offset])
        print(outputStr)
        begAddress += 8
        offset = 8-offset

        #create the rest of the memory
        while(begAddress <= endAddress):
            outputStr = " " + "{: <6}".format(byteToString(begAddress))
            outputStr = outputStr + bytearrayToString(bytearrayData[offset:offset+8])
            print(outputStr)
            begAddress += 8
            offset+=8

        return

    #this will decode the input and return instructions on what to do
    def __decodeInputData(self, inputData):
        returnData = [];

        #get a group of data
        if "." in inputData:
            inputData = inputData.split(".")

            #check the input!!
            if not self.__checkHexSingle(inputData[0]):
                return()
            if not self.__checkHexSingle(inputData[1]):
                return()
            if int(inputData[0], 16) > int(inputData[1], 16):
                return()

            #append the instruction code and the needed data to the return
            returnData.append("PrintGroup")
            returnData.extend(inputData)
            return returnData

        #set a group of data
        elif ":" in inputData:
                inputData = inputData.split(":")

                #check the input!!
                if not self.__checkHexSingle(inputData[0]):
                    return
                if not self.__checkHexMultiple(inputData[1]):
                    return

                #append the instruction code and the needed data to the return
                returnData.append("SetChunk")
                returnData.extend(inputData)
                return returnData

        #run a program from an address
        elif "R" in inputData:
            inputData = inputData[:-1]

            #check the input!!
            if not self.__checkHexSingle(inputData):
                return

            #append the instruction code and the needed data to the return
            returnData.append("RunProg")
            returnData.append(inputData)
            return returnData

        #display a single memory location
        elif self.__checkHexSingle(inputData):
            returnData.append("PrintSingle")
            returnData.append(inputData)
            return returnData

        return

    #This will check if a string is in hex format
    def __checkHexSingle(self, hexStr):
        try:
            #try converting to int
            int(hexStr, 16)
            return True
        except:
            #if nothing works return false
            return False

    def __checkHexMultiple(self, hexStr):
        try:
            #try converting to a bytearray
            bytearray.fromhex( hexStr )
            return True
        except:
            #if nothing works return false
            return False
